Tux est un mini jeu 3D pour un enfant apprenant à lire.

Créé en 2022 avec java et XML

Le but est de déplacer un petit personnage dans un environnement 3D afin d'y récolter des lettres pour former un mot.
Le moteur 3D utilisé est Env3D. Les mots sont de 5 difficultés différentes et sont importés à partir d'un fichier XML : le joueur entre son pseudo, choisit la difficulté et trouve les lettres avant la fin du temps imparti.

