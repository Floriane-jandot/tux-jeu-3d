<?xml version="1.0" encoding="UTF-8"?>


<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0"
                            xmlns:dic="http://myGame/tux"
                            xmlns:xh="http://www.w3.org/1999/xhtml">
    <xsl:output method="html"/>

   <!-- Affichage des mots et de leur niveau -->
    <xsl:template match="/">
        <html>
            <head>
                <title>dico.xsl</title>
            </head>
            <body>
                <xsl:apply-templates select="//dic:mot">
                     <xsl:sort select ="@niveau"/>
                    <xsl:sort select="."/>
                </xsl:apply-templates>
            </body>
        </html>
    </xsl:template>
    
   
    <xsl:template match="dic:mot">
                 <xsl:value-of select="."/>
                 <xsl:value-of select="@niveau"/>
                <br/>
     </xsl:template>

</xsl:stylesheet>
