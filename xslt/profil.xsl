<?xml version="1.0" encoding="UTF-8"?>


<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0"
                xmlns:pro="http://myGame/tux">
    <xsl:output method="html"/>

    
    <xsl:template match="/">
        <html>
            <head>
                <title>profil.xsl</title>
            </head>
            <body>
                
                <xsl:variable name="image" select="//pro:avatar"/>
                
                <p>Le joueur  <xsl:value-of select="//pro:nom"/>, </p>
                <img src= "../../img/{//pro:avatar} "/>
                <p> né le <xsl:value-of select="//pro:dateAnniv/text()"/>, </p> 
                <p> a réalisé les scores suivants : </p>
                
                <!-- tableau-->
                <table>
                    <tr>
                        <th>Date</th>
                        <th>Temps (s)</th>
                        <th>Mot</th>
                    </tr>
                    
                    <!-- On appelle la template pour afficher la liste des parties -->
                    <xsl:apply-templates select="//pro:partie">
                        <xsl:sort select="@date" order="ascending"/>
                    </xsl:apply-templates>
                </table>
                
            </body>
        </html>
    </xsl:template>



    <!-- TEMPLATE DE SCORES D'UNE PARTIE-->
    <xsl:template match="pro:partie">
        
        <tr>
            <th>
                <xsl:value-of select="@date"/>
            </th>
            <th>
                <xsl:value-of select="pro:temps/text()"/>
            </th>
            <th>
                <th>
                    <xsl:value-of select="pro:mot/text()"/>
                </th>
            </th>
        </tr>

    </xsl:template>

</xsl:stylesheet>
