/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package game;

/**
 *
 * @author ASUS
 */
public class Room {
    
    private int depth;
    private int height;
    private int width;
    private String textureBottom;
    private String textureNorth;
    private String textureEast;
    private String textureWest;
    private String textureTop;
    private String textureSouth;
    
    int[][] sol;
    
    public Room(){
        this.depth = 100;
        this.width = 100;
        this.height = 60;
        this. textureBottom = "textures/herbe.png"; 
        this.textureNorth = "textures/foret.jpg";
        this.textureEast = "textures/foret.jpg";
        this.textureWest = "textures/foret.jpg";
        
        
        /* 
        GESTION DES COLLISIONS
        Déclaration du tableau sol, qui possède 100 cases en tout : 10 x 10     (10 est le côté d'un cube lettre)
        */
        sol = new int[9][9];
        
        for(int i=0; i<9; i++){
            for(int j=0; j<9; j++){
                sol[i][j] = -1;
            }
        }
        
    }
    
    //GETTERS

    public int getDepth() {
        return depth;
    }
    public int getHeight() {
        return height;
    }
    public int getWidth() {
        return width;
    }
    public String getTextureBottom() {
        return textureBottom;
    }
    public String getTextureNorth() {
        return textureNorth;
    }
    public String getTextureEast() {
        return textureEast;
    }
    public String getTextureWest() {
        return textureWest;
    }
    public String getTextureTop() {
        return textureTop;
    }
    public String getTextureSouth() {
        return textureSouth;
    }
    public int[][] getSol() {
        return sol;
    }
    
    //SETTERS

    public void setDepth(int depth) {
        this.depth = depth;
    }
    public void setHeight(int height) {
        this.height = height;
    }
    public void setWidth(int width) {
        this.width = width;
    }
    public void setTextureBottom(String textureBottom) {
        this.textureBottom = textureBottom;
    }
    public void setTextureNorth(String textureNorth) {
        this.textureNorth = textureNorth;
    }
    public void setTextureEast(String textureEast) {
        this.textureEast = textureEast;
    }
    public void setTextureWest(String textureWest) {
        this.textureWest = textureWest;
    }
    public void setTextureTop(String textureTop) {
        this.textureTop = textureTop;
    }
    public void setTextureSouth(String textureSouth) {
        this.textureSouth = textureSouth;
    }
    public void setSol(int posLongueur, int posLargeur, int val) {
        sol[posLongueur][posLargeur] = val;
    }
    
    
}
