/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package game;

import env3d.Env;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Random;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerException;
import org.lwjgl.input.Keyboard;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.xml.sax.SAXException;

/**
 *
 * @author gladen
 */
public abstract class Jeu {

    enum MENU_VAL {
        MENU_SORTIE, MENU_CONTINUE, MENU_JOUE
    }

    private final Env env;
    private Tux tux;
    private final Room mainRoom;
    private final Room menuRoom;
    private Letter letter;

    private Profil profil;
    private final Dico dico;
    protected EnvTextMap menuText;                         //text (affichage des texte du jeu)
    ArrayList<Letter> lettres;
    
    
    
    public Jeu() throws SAXException, IOException, ParserConfigurationException, TransformerException {

        // Crée un nouvel environnement
        env = new Env();

        // Instancie une Room
        mainRoom = new Room();

        // Instancie une autre Room pour les menus
        menuRoom = new Room();
        menuRoom.setTextureEast("textures/black.png");
        menuRoom.setTextureWest("textures/black.png");
        menuRoom.setTextureNorth("textures/black.png");
        menuRoom.setTextureBottom("textures/black.png");

        // Règle la camera
        env.setCameraXYZ(50, 60, 175);
        env.setCameraPitch(-20);

        // Désactive les contrôles par défaut
        env.setDefaultControl(false);
        
        //Instancie un conteneur de lettres
        lettres = new ArrayList<Letter>();
        
        // Dictionnaire
        dico = new Dico("pathtodicofile");

        // instancie le menuText
        menuText = new EnvTextMap(env);
        
        // Textes affichés à l'écran
        menuText.addText("Voulez vous ?", "Question", 200, 300);
        menuText.addText("1. Commencer une nouvelle partie ?", "Jeu1", 250, 280);
        menuText.addText("2. Charger une partie existante ?", "Jeu2", 250, 260);
        menuText.addText("3. Sortir de ce jeu ?", "Jeu3", 250, 240);
        menuText.addText("4. Quitter le jeu ?", "Jeu4", 250, 220);
        menuText.addText("Choisissez un nom de joueur : ", "NomJoueur", 200, 300);
        menuText.addText("Choisissez un niveau : ", "Niveau", 200, 300);
        menuText.addText("1. Charger un profil de joueur existant ?", "Principal1", 250, 280);
        menuText.addText("2. Créer un nouveau joueur ?", "Principal2", 250, 260);
        menuText.addText("3. Sortir du jeu ?", "Principal3", 250, 240);
    }

    /**
     * Gère le menu principal
     *
     */
    public void execute() throws SAXException, IOException, ParserConfigurationException, TransformerException {
        MENU_VAL mainLoop;
        mainLoop = MENU_VAL.MENU_SORTIE;
        do {
            mainLoop = menuPrincipal();
        } while (mainLoop != MENU_VAL.MENU_SORTIE);
        this.env.setDisplayStr("Au revoir !", 300, 30);
        env.exit();
    }


    // fourni
    private String getNomJoueur() {
        String nomJoueur = "";
        menuText.getText("NomJoueur").display();
        nomJoueur = menuText.getText("NomJoueur").lire(true);
        menuText.getText("NomJoueur").clean();
        return nomJoueur;
    }
    
    private int getNiveau() {
        int niveau = 1;
        menuText.getText("Niveau").display();
        niveau = Integer.parseInt(menuText.getText("Niveau").lire(true));
        menuText.getText("Niveau").clean();
        return niveau;
    }

    
    // fourni, à compléter
    private MENU_VAL menuJeu() throws SAXException, IOException, ParserConfigurationException, TransformerException {

        MENU_VAL playTheGame;
        playTheGame = MENU_VAL.MENU_JOUE;
        Partie partie;
        int niveau;
        do {
            // restaure la room du menu
            env.setRoom(menuRoom);
            // affiche menu
            menuText.getText("Question").display();
            menuText.getText("Jeu1").display();
            menuText.getText("Jeu2").display();
            menuText.getText("Jeu3").display();
            menuText.getText("Jeu4").display();
            
            // vérifie qu'une touche 1, 2, 3 ou 4 est pressée
            int touche = 0;
            while (!(touche == Keyboard.KEY_1 || touche == Keyboard.KEY_2 || touche == Keyboard.KEY_3 || touche == Keyboard.KEY_4)) {
                touche = env.getKey();
                env.advanceOneFrame();
            }

            // nettoie l'environnement du texte
            menuText.getText("Question").clean();
            menuText.getText("Jeu1").clean();
            menuText.getText("Jeu2").clean();
            menuText.getText("Jeu3").clean();
            menuText.getText("Jeu4").clean();

            // et décide quoi faire en fonction de la touche pressée
            switch (touche) {
                // -----------------------------------------
                // Touche 1 : Commencer une nouvelle partie
                // -----------------------------------------                
                case Keyboard.KEY_1: // choisit un niveau et charge un mot depuis le dico
                    //demande le niveau souhaité
                    niveau = getNiveau();
                    // restaure la room du jeu
                    env.setRoom(mainRoom);
                    
                    dico.lireDictionnaireDOM("src/xml/", "dico.xml");
                    
                    //Récupération et création des lettres 
                    String mot = dico.getMotDepuisListeNiveaux(niveau);
                    int posLongueur;
                    int posLargeur;
                    for (int i=0; i<mot.length(); i++){
                        posLongueur = genereRandom();
                        posLargeur = genereRandom();
                        //vérification qu'il n'y ait pas déjà quelque chose sur le sol
                        while (mainRoom.getSol()[posLongueur][posLargeur] != -1){ 
                            posLongueur = genereRandom();
                            posLargeur = genereRandom();
                        }
                        //positionnement des lettres sur le quadrillage invisible du sol
                        mainRoom.setSol(posLongueur, posLargeur, i);
                        //ajout des lettres en position aléatoire à la liste
                        lettres.add(new Letter(mot.charAt(i), posLongueur*10, posLargeur*10));
                     }

                    // crée une nouvelle partie
                    partie = new Partie(java.time.LocalDate.now().toString(), getMot(), 20);
                    // joue
                    joue(partie);
                    // enregistre la partie dans le profil --> enregistre le profil
                    profil.getParties().add(partie);
                    
                    playTheGame = MENU_VAL.MENU_JOUE;
                    break;

                // -----------------------------------------
                // Touche 2 : Charger une partie existante
                // -----------------------------------------                
                 //ATTENTION, ÇA N'A PAS ÉTÉ CODÉ
                case Keyboard.KEY_2: // charge une partie existante
                    // restaure la room du jeu
                    env.setRoom(mainRoom);
                    
                    partie = new Partie("2018-09-7", "test", 1); //XXXXXXXXX
                    // Recupère le mot de la partie existante
                    // ..........
                    // joue
                    joue(partie);
                    // enregistre la partie dans le profil --> enregistre le profil
                    // .......... profil.******
                    playTheGame = MENU_VAL.MENU_JOUE;
                    break;

                // -----------------------------------------
                // Touche 3 : Sortie de ce jeu
                // -----------------------------------------                
                case Keyboard.KEY_3:
                    playTheGame = MENU_VAL.MENU_CONTINUE;
                    break;

                // -----------------------------------------
                // Touche 4 : Quitter le jeu
                // -----------------------------------------                
                case Keyboard.KEY_4:
                    playTheGame = MENU_VAL.MENU_SORTIE;
            }
        } while (playTheGame == MENU_VAL.MENU_JOUE);
        return playTheGame;
    }

    private MENU_VAL menuPrincipal() throws SAXException, IOException, ParserConfigurationException, TransformerException {

        MENU_VAL choix = MENU_VAL.MENU_CONTINUE;
        String nomJoueur;

        // restaure la room du menu
        env.setRoom(menuRoom);

        menuText.getText("Question").display();
        menuText.getText("Principal1").display();
        menuText.getText("Principal2").display();
        menuText.getText("Principal3").display();
               
        // vérifie qu'une touche 1, 2 ou 3 est pressée
        int touche = 0;
        while (!(touche == Keyboard.KEY_1 || touche == Keyboard.KEY_2 || touche == Keyboard.KEY_3)) {
            touche = env.getKey();
            env.advanceOneFrame();
        }

        menuText.getText("Question").clean();
        menuText.getText("Principal1").clean();
        menuText.getText("Principal2").clean();
        menuText.getText("Principal3").clean();

        // et décide quoi faire en fonction de la touche pressée
        switch (touche) {
            // -------------------------------------
            // Touche 1 : Charger un profil existant
            // -------------------------------------
            case Keyboard.KEY_1:
                // demande le nom du joueur existant
                nomJoueur = getNomJoueur();
                // charge le profil de ce joueur si possible
                if (profil.charge(nomJoueur)) {
                    choix = menuJeu(); 
                } else {
                    choix = MENU_VAL.MENU_SORTIE;//CONTINUE;
                }
                break;

            // -------------------------------------
            // Touche 2 : Créer un nouveau joueur
            // -------------------------------------
            case Keyboard.KEY_2:
                // demande le nom du nouveau joueur
                nomJoueur = getNomJoueur();
                
                // crée un profil avec le nom d'un nouveau joueur
                profil = new Profil(nomJoueur, "2000-01-01");
                choix = menuJeu();
                break;

            // -------------------------------------
            // Touche 3 : Sortir du jeu
            // -------------------------------------
            case Keyboard.KEY_3:
                choix = MENU_VAL.MENU_SORTIE;
        }
        return choix;
    }

    public void joue(Partie partie){

        // Instancie un Tux
        tux = new Tux(env, mainRoom);
        env.addObject(tux);

        // Ici, on peut initialiser des valeurs pour une nouvelle partie
        démarrePartie(partie);
        
        //Ajout des lettres dans l'environnement
        for (Letter l : lettres) { 		      
           env.addObject(l);	
      }
        //affichage du mot à trouver
        int indiceLettre = 0;
        env.setDisplayStr(getMot(), 280, 320);
        
        //paramètres du volume
        env.setVolume(0.5);
        
        
        // Boucle de jeu
        Boolean finished;
        finished = false;
        while (!finished) {

            // Contrôles globaux du jeu (sortie, ...)
            //1 is for escape key
            if (env.getKey() == 1) {
                finished = true;
            }

            // Contrôles des déplacements de Tux (gauche, droite, ...)
            tux.déplace();

            // Ici, on applique les regles
            if (appliqueRegles(partie)){
                env.soundPlay("sounds/Pikachu.wav");
                env.removeObject(lettres.get(indiceLettre));
                indiceLettre++;
            }

            // Fait avancer le moteur de jeu (mise à jour de l'affichage, de l'écoute des événements clavier...)
            env.advanceOneFrame();
        }

        // Ici on peut calculer des valeurs lorsque la partie est terminée
        terminePartie(partie);

    }

    protected abstract void démarrePartie(Partie partie);

    protected abstract boolean appliqueRegles(Partie partie);

    protected abstract void terminePartie(Partie partie);

    public ArrayList<Letter> getLettres() {
        return lettres;
    }
    
    //génère un nombre aléatoire entre 0 et 9
    public int genereRandom(){
        Random r = new Random();
        return  r.nextInt(9); 
    }
    
    //renvoie la case sur laquelle tux se trouve : si c'est -1, il n'y a rien
    protected int collision(){ 
        int caseSol = -1;
        int posXpika = (int) (tux.getX()/10);
        int posZpika = (int) (tux.getZ()/10);
        
        try{
            if (mainRoom.getSol()[posXpika][posZpika] != -1){
                System.out.println("Une lettre a été percutée !");
                caseSol = mainRoom.getSol()[posXpika][posZpika];
                mainRoom.getSol()[posXpika][posZpika]=-1;
            }
        }
        catch(ArrayIndexOutOfBoundsException e){}
        
        return caseSol ;
    }
    
    String getMot(){
        String mot = "";
        for (Letter l: lettres){
            mot = mot + l.getLetter();
        }
        return mot;
    }
    
    public Env getEnv() {
        return env;
    }

    public Profil getProfil() {
        return profil;
    }
    
}
