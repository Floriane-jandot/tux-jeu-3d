/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package game;

import java.io.IOException;
import java.util.concurrent.TimeUnit;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerException;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.xml.sax.SAXException;

/**
 *
 * @author ASUS
 */
public class JeuDevineLeMotOrdre extends Jeu {
    
    int nbLettresRestantes;
    Chronometre chrono;
    
    //définit le temps : 20 secondes
    public JeuDevineLeMotOrdre() throws SAXException, IOException, ParserConfigurationException, TransformerException{
        super();
        this.chrono = new Chronometre(20);
    }
    
    //initialise le nombre de lettres du mot et lance le chronomètre
    @Override
    protected void démarrePartie(Partie partie){
        nbLettresRestantes = lettres.size();
        chrono.start();
    }
    
//applique les règles de jeu. Si tux a rencontré une lettre, elle renvoie true à la fonctoin appelante pour lui dire de la faire disparaître
    @Override
    protected boolean appliqueRegles(Partie partie){
        boolean detruireLettre = tuxTrouveLettre();
        if (nbLettresRestantes == 0 || !(chrono.remainsTime()) ){
            terminePartie(partie);
        }
        return detruireLettre;
    }
    
    //arrête le chronomètre et calcule le pourcentage de lettres trouvées, puis coupe le jeu
    //--NE MARCHE PAS- : ajoute au document xml du joueur la partie qu'il vient d'effectuer
    @Override
    protected void terminePartie(Partie partie){
        chrono.stop();
        partie.setTemps((int) chrono.getTime());
        
        int totalLettresTrouvees = lettres.size() - nbLettresRestantes;
        float division =  (float) totalLettresTrouvees  / lettres.size();
        int pourcentage = (int) (division * 100) ;
        partie.setTrouve(pourcentage);
        System.out.println("Le pourcentage du mot trouvé est de " + pourcentage + "%.");
        
        String filename = "src/xml/" + getProfil().getNom() + ".xml";
        Document doc = getProfil().fromXML(filename);
        Element partie_a_ajouter = partie.getPartie(doc);
        Element parties = (Element)doc.getElementsByTagName("parties").item(0);
        parties.appendChild(partie_a_ajouter);
        
        getEnv().soundPlay("sounds/HappyPika.wav");
        try {
            TimeUnit.SECONDS.sleep(2);
        } catch (InterruptedException ex) {
            Logger.getLogger(JeuDevineLeMotOrdre.class.getName()).log(Level.SEVERE, null, ex);
        }
        System.exit(0);
    }
    
//renvoie true si tux bute une lettre
    private boolean tuxTrouveLettre(){
        boolean trouve = false;
        if (collision() ==getLettres().size() - nbLettresRestantes){
            nbLettresRestantes--;
            trouve = true;
        }
        return trouve;
    }
    
    private int getNbLettresRestantes(){
        return nbLettresRestantes;
    }
    
    private int getTemps(){
        return (int) chrono.getTime();
    }
            
}
