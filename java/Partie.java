/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package game;

import org.w3c.dom.*;

/**
 *
 * @author ASUS
 */
public class Partie {
    
    private String date;
    private String mot;
    private int niveau;
    private int trouvé;
    private int temps;

    public Partie(String date, String mot, int niveau) {
        this.date = date;
        this.mot = mot;
        this.niveau = niveau;
    }
    
    //parsing : récupération des éléments
    public Partie(Element partieElt){
        this.date = partieElt.getAttribute("date");
        this.trouvé = Integer.valueOf(partieElt.getAttribute("trouvé"));
        this.temps = Integer.valueOf(partieElt.getElementsByTagName("temps").item(0).getTextContent());

        Element e_mot = (Element)partieElt.getElementsByTagName("mot").item(0);
        this.mot = e_mot.getTextContent();
        this.niveau = Integer.valueOf(e_mot.getAttribute("niveau"));
    }
    
 //renvoie un bloc XML d'une partie 
    public Element getPartie(Document doc){
        Element racine = doc.createElement("partie");
        
        Node n_date = doc.createElement("date");
        n_date.appendChild(doc.createTextNode(date));
        racine.appendChild(n_date);
        
        Node n_mot = doc.createElement("mot");
        n_mot.appendChild(doc.createTextNode(mot));
        racine.appendChild(n_mot);
        
        Node n_niveau = doc.createElement("niveau");
        n_niveau.appendChild(doc.createTextNode(Integer.toString(niveau)));
        racine.appendChild(n_niveau);
        
        Node n_trouve = doc.createElement("trouvé");
        n_trouve.appendChild(doc.createTextNode(Integer.toString(trouvé)));
        racine.appendChild(n_trouve);
        
        Node n_temps = doc.createElement("temps");
        n_temps.appendChild(doc.createTextNode(Integer.toString(temps)));
        racine.appendChild(n_temps);
        
        doc.appendChild(racine);
        
        return racine;
    }

    public void setTrouve(int trouvé) {
        this.trouvé = trouvé;
    }

    public void setTemps(int temps) {
        this.temps = temps;
    }

    public int getNiveau() {
        return niveau;
    }
    
    public String toString(){
        return "";
    }
    
    
    
}
