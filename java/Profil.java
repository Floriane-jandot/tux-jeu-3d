package game;

import com.sun.org.apache.xerces.internal.parsers.DOMParser;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.xml.sax.SAXException;

import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import org.w3c.dom.Document;
import org.w3c.dom.NodeList;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author ASUS
 */
public class Profil {
    
    private String nom;
    private String dateNaissance;
    private String avatar;
    private  ArrayList<Partie> parties;
    public Document _doc;

    //ce constructeur est appelé dans le cadre d'un nouveau joueur
    //il crée un nouveau fichier xml d'un joueur qui sera nommé <nom>.xml, dans lequel on placera son nom, une date de naissance et un avatar
    //à la fin de chaque partie jouée par ce joueur, on le réouvre pour ajouter les résultats
    public Profil(String nom, String dateNaissance) throws ParserConfigurationException, TransformerConfigurationException, TransformerException {
        parties = new ArrayList<Partie>();
        this.nom = nom;
        this.dateNaissance = dateNaissance;
        
        DocumentBuilderFactory documentFactory = DocumentBuilderFactory.newInstance();
        DocumentBuilder documentBuilder = documentFactory.newDocumentBuilder();
        Document document = documentBuilder.newDocument();

        // racine
        Element racine = document.createElement("profil");
        document.appendChild(racine);

        // elements
        Element e_nom = document.createElement("nom");
        e_nom.appendChild(document.createTextNode(nom));
        racine.appendChild(e_nom);
        Element e_avatar = document.createElement("avatar");
        e_avatar.appendChild(document.createTextNode("conan.jpg"));
        racine.appendChild(e_avatar);
        Element e_dateAnniv = document.createElement("dateAnniv");
        e_dateAnniv.appendChild(document.createTextNode(dateNaissance));
        racine.appendChild(e_dateAnniv);
        Element e_parties = document.createElement("parties");
        e_dateAnniv.appendChild(document.createTextNode(""));
        racine.appendChild(e_parties);
        
        //création du fichier xml
        TransformerFactory transformerFactory = TransformerFactory.newInstance();
        Transformer transformer = transformerFactory.newTransformer();
        DOMSource domSource = new DOMSource(document);
        StreamResult streamResult = new StreamResult(new File("src/xml/" + nom + ".xml"));
        transformer.transform(domSource, streamResult);

        System.out.println("Fichier XML créé.");
        
        
    }
    
    //ce constructeur est appelé si le joueur existe déjà
    //on cherche à extraire les informations déjà connues dans son fichier XML
    public Profil (String filename) throws SAXException, IOException{
        parties = new ArrayList<Partie>();
        
        _doc = fromXML(filename);
        
        this.nom = _doc.getElementsByTagName("nom").item(0).getTextContent();
        this.dateNaissance = _doc.getElementsByTagName("dateAnniv").item(0).getTextContent();
        this.avatar = _doc.getElementsByTagName("avatar").item(0).getTextContent();
        
        int nbParties = _doc.getElementsByTagName("partie").getLength();
        
        for (int i=0; i<nbParties; i++){
            Element partieElt = (Element)_doc.getElementsByTagName("partie").item(i);
            ajouterPartie(new Partie(partieElt));
        }
    }
    
    // Cree un DOM à partir d'un fichier XML
    public Document fromXML(String nomFichier) {
        try {
            return XMLUtil.DocumentFactory.fromFile(nomFichier);
        } catch (Exception ex) {
            Logger.getLogger(Profil.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    // Sauvegarde un DOM en XML
    public void toXML(String nomFichier) {
        try {
            XMLUtil.DocumentTransform.writeDoc(_doc, nomFichier);
        } catch (Exception ex) {
            Logger.getLogger(Profil.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    //merci pour cette fonction mais je n'ai pas eu le temps de m'en occuper !
    /// Takes a date in XML format (i.e. ????-??-??) and returns a date
    /// in profile format: dd/mm/yyyy
    public static String xmlDateToProfileDate(String xmlDate) {
        String date;
        // récupérer le jour
        date = xmlDate.substring(xmlDate.lastIndexOf("-") + 1, xmlDate.length());
        date += "/";
        // récupérer le mois
        date += xmlDate.substring(xmlDate.indexOf("-") + 1, xmlDate.lastIndexOf("-"));
        date += "/";
        // récupérer l'année
        date += xmlDate.substring(0, xmlDate.indexOf("-"));

        return date;
    }

    /// Takes a date in profile format: dd/mm/yyyy and returns a date
    /// in XML format (i.e. ????-??-??)
    public static String profileDateToXmlDate(String profileDate) {
        String date;
        // Récupérer l'année
        date = profileDate.substring(profileDate.lastIndexOf("/") + 1, profileDate.length());
        date += "-";
        // Récupérer  le mois
        date += profileDate.substring(profileDate.indexOf("/") + 1, profileDate.lastIndexOf("/"));
        date += "-";
        // Récupérer le jour
        date += profileDate.substring(0, profileDate.indexOf("/"));

        return date;
    }
    
    public boolean charge(String nomJoueur){
        return false;
    }
    
    public ArrayList<Partie> getParties() {
        return parties;
    }
    
    public void ajouterPartie(Partie p){
        parties.add(p);
    }

    public String getNom() {
        return nom;
    }
    
    
}
