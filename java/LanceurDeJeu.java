/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package game;

import java.io.IOException;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerException;
import org.xml.sax.SAXException;

/**
 *
 * @author ASUS
 */
public class LanceurDeJeu {
    
    public static void main(String[] args) throws SAXException, IOException, ParserConfigurationException, TransformerException {
        // Declare un Jeu
        JeuDevineLeMotOrdre jeu;
        //Instancie un nouveau jeu
        jeu = new JeuDevineLeMotOrdre();
        //Execute le jeu
        jeu.execute();
    }
    
}
