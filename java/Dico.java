/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package game;

import java.util.ArrayList;
import java.util.Random;
import org.w3c.dom.*;
import java.io.File;
import java.io.IOException;
import java.io.StringWriter;
import javax.xml.parsers.*;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import org.xml.sax.SAXException;



/**
 *
 * @author ASUS
 */
public class Dico {
    //on a une liste de mots par liveau et une liste générale qui les englobe toutes
    private ArrayList<String> listeNiveau1;
    private ArrayList<String> listeNiveau2;
    private ArrayList<String> listeNiveau3;
    private ArrayList<String> listeNiveau4;
    private ArrayList<String> listeNiveau5;
    private ArrayList<ArrayList<String> > toutesLesListes;
    
    private String cheminFichierDico;
    
    public Dico(String c){
        this.cheminFichierDico = c;
        listeNiveau1 = new ArrayList<String>();
        listeNiveau2 = new ArrayList<String>();
        listeNiveau3 = new ArrayList<String>();
        listeNiveau4 = new ArrayList<String>();
        listeNiveau5 = new ArrayList<String>();  
        
        toutesLesListes =new ArrayList<ArrayList<String> >(5);
        
        toutesLesListes.add(listeNiveau1);
        toutesLesListes.add(listeNiveau2);
        toutesLesListes.add(listeNiveau3);
        toutesLesListes.add(listeNiveau4);
        toutesLesListes.add(listeNiveau5);
    }
    
    //si le niveau entré par le joueur est non-conforme, on l'initialise à 1
    public String getMotDepuisListeNiveaux(int niveau){
        if (niveau < 1 || niveau > 5){
            niveau = 1;
            System.out.println("Aïe, le niveau doit être entre 1 et 5. Le niveau par défaut 1 a été selectionné.");
        }
        
        String mot = getMotDepuisListe(toutesLesListes.get(niveau-1));
        
        return mot;
    }
    
    public void ajouteMotADico(int niveau, String mot){
        toutesLesListes.get(niveau).add(mot);
    }
    
    //au stade de mon code, cette fonction ne sert à rien
    public String getCheminFichierDico(){
        return null;
    }
    
    private int verifieNiveau(int niveau){
        return 0;
    }
    
    //retourne un mot aléatoire d'une liste de niveau, ou "vide" s'il y a une erreur 
    private String getMotDepuisListe(ArrayList<String> list){
        if (list.isEmpty()){
            return "vide";
        }
        else{
            Random r = new Random();
            return(list.get(r.nextInt(list.size()-0)) );
        }
    }
    
    //ouvre dico.xml, extrait les mots et les place dans leur liste respective
    //ici, j'ai joué avec le fait qu'on avait toujours 3 mots par niveau, mais on aurait pu aussi extraire l'attribut niveau pour les placer.
    public void lireDictionnaireDOM(String path, String filename) throws SAXException, IOException, ParserConfigurationException, TransformerException{
        Document doc;
        File xmlFile = new File(path + filename);
        
        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        DocumentBuilder dBuilder = factory.newDocumentBuilder();
        doc = dBuilder.parse(xmlFile);
        
        //System.out.println( doc.getElementsByTagName("ns1:mot").item(0).getTextContent() );
        //5 niveaux
        int compterMot = 0;
        for (int i=0; i<5; i++){
            //3 mots par niveau
            for (int j=0; j<3; j++){
                ajouteMotADico(i, doc.getElementsByTagName("ns1:mot").item(compterMot).getTextContent());
                compterMot++;
            }
        }
        
    }
    
}
