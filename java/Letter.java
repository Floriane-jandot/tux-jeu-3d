/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package game;
import env3d.advanced.EnvNode;
import env3d.Env;
/**
 *
 * @author ASUS
 */
public class Letter extends EnvNode {
    
    private char letter;
    
    //initialise les caractéristiques d'un cube lettre
    public Letter(char l, double x, double y){
        this.letter = l;
        
        setScale(4.0);
        setX(x);// positionnement au milieu de la largeur de la room
        setY(8); // positionnement en hauteur basé sur la taille de Tux
        setZ(y);
        if ( l != ' '){
             setTexture("textures/letter/letter/"+letter+".png");
        }
        else{
            setTexture("models/cube/cube.png");
        }
        setModel("models/cube/cube.obj");  
    } 

    public char getLetter() {
        return letter;
    }
    
            
}
